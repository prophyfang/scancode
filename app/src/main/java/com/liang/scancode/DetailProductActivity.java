package com.liang.scancode;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.liang.scancode.sql.Product;
import com.liang.scancode.sql.ProductService;

public class DetailProductActivity extends Activity {
    private Button mFinishButton;
    private Button mModeButton;
    private EditText mCustomidET;
    private EditText mNameET;
    private EditText mBarcodeET;
    private EditText mDescribetionET;
    private String currentMode = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        initProductInfoView();
        initButtonView();

        Intent intent = getIntent();
        String customid = intent.getStringExtra("customid");
        String name = intent.getStringExtra("name");
        String barcode = intent.getStringExtra("barcode");
        String describetion = intent.getStringExtra("describetion");
        mCustomidET.setText(customid);
        mNameET.setText(name);
        mBarcodeET.setText(barcode);
        mDescribetionET.setText(describetion);

        currentMode = intent.getStringExtra("mode");
        if (currentMode.equals("view")) {
            enterViewMode();
        } else if (currentMode.equals("edit")) {
            enterEditMode();
        }
    }

    private void initButtonView() {
        mFinishButton = (Button) findViewById(R.id.btn_finish);
        mFinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mModeButton = (Button) findViewById(R.id.btn_mode);
        mModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchMode();
            }
        });
    }

    private void initProductInfoView() {
        mCustomidET = (EditText) findViewById(R.id.product_customid);
        mNameET = (EditText) findViewById(R.id.product_name);
        mBarcodeET = (EditText) findViewById(R.id.product_barcode);
        mDescribetionET = (EditText) findViewById(R.id.product_describetion);
    }

    private void switchMode() {
        if (currentMode.equals("view")) {
            enterEditMode();
            currentMode = "edit";
        } else if (currentMode.equals("edit")) {
            saveProduct();
            enterViewMode();
            currentMode = "view";
        }
    }

    private void saveProduct() {
        String customid = mCustomidET.getText().toString();
        String name = mNameET.getText().toString();
        String barcode = mBarcodeET.getText().toString();
        String describetion = mDescribetionET.getText().toString();
        Product product = new Product(customid, name, barcode, describetion);
        ProductService.getInstance(this).saveProduct(product);
    }

    private void enterViewMode() {
        mCustomidET.setEnabled(false);
        mNameET.setEnabled(false);
        mDescribetionET.setEnabled(false);
        mModeButton.setText("编辑");
    }

    private void enterEditMode() {
        String customid = mCustomidET.getText().toString();
        String name = mNameET.getText().toString();
        String barcode = mBarcodeET.getText().toString();
        String describetion = mDescribetionET.getText().toString();
        mCustomidET.setEnabled(true);
        mNameET.setEnabled(true);
        mDescribetionET.setEnabled(true);
        mModeButton.setText("保存");
        mCustomidET.setText(customid);
        mNameET.setText(name);
        mBarcodeET.setText(barcode);
        mDescribetionET.setText(describetion);
    }
}
