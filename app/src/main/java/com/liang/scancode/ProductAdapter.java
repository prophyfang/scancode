package com.liang.scancode;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liang.scancode.sql.Product;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    ArrayList<Product> adapterData;


    public ProductAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public ArrayList<Product> getData() {
        return adapterData;
    }

    public void setData(ArrayList<Product> pushInfoList) {
        adapterData = pushInfoList;
    }

    @Override
    public int getCount() {
        return (null == adapterData ? 0 : adapterData.size());
    }

    @Override
    public Object getItem(int position) {
        return (null == adapterData ? null : adapterData.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        pushViewHolder aholder = null;
        Product product = adapterData.get(position);
        if (convertView == null) {
            aholder = new pushViewHolder();
            convertView = mInflater.inflate(R.layout.item_product, null);
            aholder.nameTV = (TextView) convertView
                    .findViewById(R.id.name);
            aholder.customidTV = (TextView) convertView
                    .findViewById(R.id.customid);
            convertView.setTag(aholder);
        } else {
            aholder = (pushViewHolder) convertView.getTag();
        }

        aholder.nameTV.setText(product.getName());
        aholder.customidTV.setText(product.getCustomid());
        return convertView;
    }

    class pushViewHolder {
        TextView nameTV;
        TextView customidTV;
    }

}
