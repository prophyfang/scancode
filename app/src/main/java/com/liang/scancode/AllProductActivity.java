package com.liang.scancode;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.liang.scancode.sql.Product;
import com.liang.scancode.sql.ProductService;


public class AllProductActivity extends Activity {
    private Button mFinishActivity;

    private LinearLayout mBottomLoadLayout;// 加载提示的布局
    private LinearLayout mTopLoadLayout;// 加载提示的布局
    private TextView mBottomLoadInfo;// 加载提示
    private TextView mTopLoadInfo;// 加载提示
    private ListView mProductListView;// 列表
    private ProductService mProductService;
    private ProductAdapter mAdapter;// 列表适配器

    private int mProductCurrentPage = 1;// 默认第一页
    private final int mProductLineSize = 10;// 每次显示数
    private int mAllRecorders = 0;// 全部记录数
    private int mPageSize = 1;// 默认共1页
    private boolean bIsLast = false;// 是否最后一条
    private int mFirstItem;// 第一条显示出来的数据的游标
    private int mLastItem;// 最后显示出来数据的游标
    private boolean bIsUpdate = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allproduct);
        initProductListView();
        initButtonView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        updateProductList();
    }

    private void initButtonView() {
        mFinishActivity = (Button) findViewById(R.id.finish_activity);
        mFinishActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initProductListView() {
        mProductService = ProductService.getInstance(this);
        mProductListView = (ListView) findViewById(R.id.product_list);

        mProductListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                mFirstItem = firstVisibleItem;
                mLastItem = totalItemCount - 1;
                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    bIsLast = true;
                } else {
                    bIsLast = false;
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 是否到最底部并且数据还没读完
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    if (bIsLast && mProductCurrentPage < mPageSize) {
                        mProductCurrentPage++;
                        // 设置显示位置
                        mProductListView.setSelection(mLastItem);
                        // 增加数据
                        appendProducts();
                    } else if (mFirstItem == 0) {
                        if (bIsUpdate && mTopLoadInfo.getHeight() >= 50) {
                            bIsUpdate = false;
                            updateProducts();
                            TranslateAnimation alp = new TranslateAnimation(0, 0, 80, 0);
                            alp.setDuration(1000);
                            alp.setRepeatCount(1);
                            mTopLoadLayout.setAnimation(alp);
                            alp.setAnimationListener(new Animation.AnimationListener() {

                                @Override
                                public void onAnimationStart(Animation animation) {
                                    mTopLoadInfo.setText("正在更新...");
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    mTopLoadInfo.setText("更多新消息...");
                                    mTopLoadLayout.setVisibility(View.GONE);
                                    mTopLoadInfo.setHeight(0);
                                    mTopLoadLayout.setMinimumHeight(0);
                                }
                            });
                        }
                    }
                } else if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL
                        && mFirstItem == 0) {
                    if (mTopLoadInfo.getHeight() < 50) {
                        bIsUpdate = true;
                        mTopLoadInfo.setHeight(50);
                        mTopLoadLayout.setMinimumHeight(100);
                        mTopLoadLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

        });

        mProductListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //添加了header view所以需要减掉1
                Product product = (Product) mAdapter.getItem(i - 1);
                Intent intent = new Intent(AllProductActivity.this, DetailProductActivity.class);
                intent.putExtra("customid", product.getCustomid());
                intent.putExtra("name", product.getName());
                intent.putExtra("barcode", product.getBarcode());
                intent.putExtra("describetion", product.getDescribetion());
                intent.putExtra("mode", "view");
                startActivityForResult(intent, 0);
            }
        });

        //创建一个角标线性布局来显示正在加载
        mBottomLoadLayout = new LinearLayout(this);
        mBottomLoadLayout.setMinimumHeight(100);
        mBottomLoadLayout.setGravity(Gravity.CENTER);

        // 定义一个文本显示"正在加载文本"
        mBottomLoadInfo = new TextView(this);
        mBottomLoadInfo.setHeight(50);
        mBottomLoadInfo.setTextSize(16);
        mBottomLoadInfo.setTextColor(getResources().getColor(R.color.gray));
        mBottomLoadInfo.setText("加载更多...");
        mBottomLoadInfo.setGravity(Gravity.CENTER);

        mBottomLoadLayout.addView(mBottomLoadInfo, new ViewGroup.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        mBottomLoadLayout.setMinimumHeight(100);
        mBottomLoadLayout.getBottom();
        mProductListView.addFooterView(mBottomLoadLayout);

        //创建一个角标线性布局来显示正在加载
        mTopLoadLayout = new LinearLayout(this);
        mTopLoadLayout.setGravity(Gravity.CENTER);
        mTopLoadLayout.setBackgroundResource(R.color.white);

        // 定义一个文本显示"正在加载文本"
        mTopLoadInfo = new TextView(this);
        mTopLoadInfo.setTextSize(14);
        mTopLoadInfo.setTextColor(getResources().getColor(R.color.gray));
        mTopLoadInfo.setText("更多新消息...");
        mTopLoadInfo.setGravity(Gravity.CENTER);
        mTopLoadInfo.setHeight(0);

        mTopLoadLayout.addView(mTopLoadInfo, new ViewGroup.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        mProductListView.addHeaderView(mTopLoadLayout);
        mTopLoadLayout.setVisibility(View.GONE);

        mAdapter = new ProductAdapter(this);
        mProductListView.setAdapter(mAdapter);
        updateProductList();
        Toast.makeText(this, "共" + mAllRecorders + "条信息,加载了" + mAdapter.getData().size() + "条信息", Toast.LENGTH_SHORT).show();
    }

    private void updateProducts() {
        int oldAllRecorders = mAllRecorders;
        updateProductList();
        Toast.makeText(this, "共" + mAllRecorders + "条信息,更新了" + (mAllRecorders - oldAllRecorders) + "条新信息", Toast.LENGTH_SHORT).show();
    }

    private void updateProductList() {
        mAllRecorders = mProductService.getCount();

        mPageSize = (mAllRecorders + mProductLineSize - 1) / mProductLineSize;

        if (mAllRecorders <= mProductLineSize) {
            mBottomLoadLayout.setVisibility(View.GONE);
        } else {
            if (mProductListView.getFooterViewsCount() < 1) {
                mBottomLoadLayout.setVisibility(View.VISIBLE);
            }
        }

        mAdapter.setData(mProductService.getScrollData(
                mProductCurrentPage = 1, mProductLineSize));
        mAdapter.notifyDataSetChanged();
    }

    private void appendProducts() {
        mAllRecorders = mProductService.getCount();
        mPageSize = (mAllRecorders + mProductLineSize - 1) / mProductLineSize;
        int oldsize = mAdapter.getData().size();
        mAdapter.getData().addAll(
                mProductService.getScrollData(mProductCurrentPage, mProductLineSize));
        if (mAllRecorders == mAdapter.getCount()) {
            mBottomLoadLayout.setVisibility(View.GONE);
        } else {
            mBottomLoadLayout.setVisibility(View.VISIBLE);
        }
        Toast.makeText(this, "共" + mAllRecorders + "条信息,加载了" + (mAdapter.getData().size() - oldsize) + "条信息", Toast.LENGTH_SHORT).show();
        mAdapter.notifyDataSetChanged();
    }

}
