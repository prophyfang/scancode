package com.liang.scancode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;


@SuppressLint("HandlerLeak")
public class ScanCodeApplication extends android.app.Application {
    private final String DB_NAME = "product.db";
    private static Context mContext;
    private static String DB_PATH;

    public static Context getContext() {
        if (mContext == null) {
            throw new RuntimeException("Unknown Error");
        }
        return mContext;
    }

    public static String getDaPath() {
        return DB_PATH;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        DB_PATH = "/data"
                + Environment.getDataDirectory().getAbsolutePath()
                + File.separator + getPackageName()
                + File.separator + DB_NAME;
        copyProductDB();
    }

    private void copyProductDB() {
        File db = new File(DB_PATH);
        if (!db.exists()) {
            try {
                InputStream is = getAssets().open(DB_NAME);
                FileOutputStream fos = new FileOutputStream(db);
                int len = -1;
                byte[] buffer = new byte[1024];
                while ((len = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                    fos.flush();
                }
                fos.close();
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

}
