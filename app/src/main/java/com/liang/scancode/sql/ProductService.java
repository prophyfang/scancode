package com.liang.scancode.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductService {
    private static ProductService instance = null;
    private SQLiteDatabase db = null;
    private DatabaseManager manager = null;

    public ProductService(Context context) {
        DatabaseManager.initializeInstance(context);
        manager = DatabaseManager.getInstance();
        db = manager.openDatabase();
    }

    public synchronized static ProductService getInstance(Context context) {
        if (null == instance) {
            instance = new ProductService(context);
        }
        return instance;
    }

    public synchronized void closeDatabase() {
        if (db != null) {
            manager.closeDatabase();
            db = null;
        }
    }

    public void saveProduct(Product product) {
        ContentValues values = new ContentValues();
        values.put("customid", product.getCustomid());
        values.put("name", product.getName());
        values.put("barcode", product.getBarcode());
        values.put("describetion", product.getDescribetion());
        if (isProductExitByBarcode(product.getBarcode())) {
            String[] args = {String.valueOf(product.getBarcode())};
            db.update("product", values, "barcode=?", args);
        } else {
            db.insert("product", null, values);
        }
    }

    public ArrayList<Product> getAllProduct() {
        Cursor cursor = db.rawQuery(
                "select * from product order by customid ASC",
                null);
        try {
            ArrayList<Product> products = new ArrayList<Product>();
            while (cursor.moveToNext()) {
                Product product = new Product(cursor.getString(cursor
                        .getColumnIndex("customid")), cursor.getString(cursor
                        .getColumnIndex("name")), cursor.getString(cursor
                        .getColumnIndex("barcode")), cursor.getString(cursor
                        .getColumnIndex("describetion")));
                products.add(product);
            }
            return products;
        } finally {
            cursor.close();
        }
    }

    public void deleteAllProduct() {
        db.delete("product", null, null);
    }

    public void deleteProductByBarcode(String barcode) {
        if (barcode == null || barcode.equals("")) {
            return;
        }
        db.delete("product", "barcode=?", new String[]{barcode});
    }

    public Product getProductByBarcode(String barcode) {
        Product product = null;
        Cursor cursor = db.rawQuery("select * from product where barcode =?", new String[]{String.valueOf(barcode)});
        try {
            if (cursor.moveToFirst()) {
                product = new Product(cursor.getString(cursor
                        .getColumnIndex("customid")), cursor.getString(cursor
                        .getColumnIndex("name")), cursor.getString(cursor
                        .getColumnIndex("barcode")), cursor.getString(cursor
                        .getColumnIndex("describetion")));
            }
        } finally {
            cursor.close();
        }
        return product;
    }

    public boolean isProductExitByBarcode(String barcode) {
        String sql = "select * from product where barcode = " + "\"" + barcode + "\"";
        Cursor cursor = db.rawQuery(sql, null);
        try {
            if (cursor.moveToFirst()) {
                cursor.close();
                return true;

            }
        } finally {
            cursor.close();
        }
        return false;
    }

    public ArrayList<Product> getScrollData(int currentPage, int lineSize) {
        String firstResult = String.valueOf((currentPage - 1) * lineSize);
        String sql = "select * from product order by customid ASC limit " + firstResult + ", " + lineSize;
        Cursor cursor = db.rawQuery(sql, null);
        try {

            ArrayList<Product> products = new ArrayList<Product>();
            while (cursor.moveToNext()) {
                Product product = new Product(cursor.getString(cursor
                        .getColumnIndex("customid")), cursor.getString(cursor
                        .getColumnIndex("name")), cursor.getString(cursor
                        .getColumnIndex("barcode")), cursor.getString(cursor
                        .getColumnIndex("describetion")));
                products.add(product);
            }
            return products;
        } finally {
            cursor.close();
        }
    }

    public int getCount() {
        Cursor cursor = db.rawQuery("select count(*) from product", null);
        try {
            cursor.moveToFirst();
            return cursor.getInt(0);
        } finally {
            cursor.close();
        }
    }

}
