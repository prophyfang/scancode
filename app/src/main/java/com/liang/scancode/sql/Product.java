package com.liang.scancode.sql;

import java.io.Serializable;

public class Product implements Serializable {
    private String customid; //自定义的id
    private String name; //商品名
    private String barcode; //条码
    private String describetion; //商品描述

    public Product() {
    }

    public Product(String customid, String name, String barcode, String describetion) {
        super();
        this.customid = customid;
        this.name = name;
        this.barcode = barcode;
        this.describetion = describetion;
    }

    public String getCustomid() {
        return customid;
    }

    public void setCustomid(String id) {
        this.customid = customid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDescribetion() {
        return describetion;
    }

    public void setDescribetion(String describetion) {
        this.describetion = describetion;
    }

    private static final long serialVersionUID = -8730643835168072945L;
}
