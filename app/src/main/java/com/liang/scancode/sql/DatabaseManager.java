package com.liang.scancode.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.liang.scancode.ScanCodeApplication;

public class DatabaseManager {

    private static DatabaseManager instance;
    private static SQLiteDatabase mDatabase;

    public static synchronized void initializeInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager();
            mDatabase = context.openOrCreateDatabase(ScanCodeApplication.getDaPath(), Context.MODE_PRIVATE, null);
        }
    }

    public static synchronized DatabaseManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(DatabaseManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }

        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {
        return mDatabase;
    }

    public synchronized void closeDatabase() {
        mDatabase.close();
    }
}
